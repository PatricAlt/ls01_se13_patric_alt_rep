﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag = 0.0;;
       double rückgabebetrag;
       int ticketanzahl=0;//int, da anzhal nur ganzzahlige zahlen sein kann

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       ticketanzahl=fahrkartenbestellungErfassen();
       System.out.println("tickets: "+ticketanzahl);
       
       if (ticketanzahl <=0)
       {
    	   ticketanzahl = 1;
       }
       
       zuZahlenderBetrag= zuZahlenderBetrag*ticketanzahl;//Es wird berechnet, der Einzelpreis MAL die Anzahl der Tickets
       
       
       System.out.println("Zu zahlender Betrag (EURO): "+zuZahlenderBetrag);
       fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben(ticketanzahl);

       tastatur.close();

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static int fahrkartenbestellungErfassen() {
    	Scanner tast2 = new Scanner(System.in);
    	int ticketanzahl = 0;
    	 System.out.print("Anzahl der Tickets: ");
         ticketanzahl = tast2.nextInt();
         tast2.close();
         
         return ticketanzahl;
    
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tast3 = new Scanner(System.in);
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	// Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   double betrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", betrag);
     	   System.out.print("Eingabe (mind. 10Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tast3.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        tast3.close();
    return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben(int ticketanzahl) {
    	// Fahrscheinausgabe
        // -----------------
        if (ticketanzahl > 1)
        {
     	 System.out.println("\nFahrscheine werden ausgegeben"); 
        } 
        else {
     	 System.out.println("\nFahrschein wird ausgegeben");
        }
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag;
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rückgabebetrag= Math.round(rückgabebetrag * 100.0) / 100.0;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
        }
    
    }
    

}
/* Operatoren.java
   Uebung zu Operatoren in Java
*/
public class Operatoren {
      public static void main(String[] args) {
            /* 1. Deklarieren Sie zwei Ganzzahlen.*/

            System.out.println("UEBUNG ZU OPERATOREN IN JAVA");
            System.out.println();

            /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
                  und geben Sie sie auf dem Bildschirm aus. */
            int a = 75;
            int b = 23;
            System.out.println(a+"und"+b);

            /* 3. Addieren Sie die Ganzzahlen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
            System.out.println(a+b);

            /* 4. Wenden Sie *alle anderen* arithmetischen Operatoren auf die
                  Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
                  Bildschirm aus. */
            	
            /* 5. Ueberpruefen Sie, ob die beiden Ganzzahlen gleich sind
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
            	System.out.println(a==b);
            /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
                  und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
            	System.out.println(a<b);
            	System.out.println(a>=b);
            /* 7. Ueberpruefen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. 
         
                  Tipp: Auch das geht nur mit Operatoren!
            */
            	System.out.println(a<=50 && a>=0);
            	System.out.println(b<=50 && b>=0);

      } //Ende von main
} // Ende von Operatoren
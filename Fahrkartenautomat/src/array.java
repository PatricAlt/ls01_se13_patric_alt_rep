/**
  *
  * Beschreibung
  *  Einf�hrung in die Programmierung mit Arrays 
  * @version 1.0 vom 19.03.2017
  * @author 
  */
import java.util.Scanner;
public class array {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    
    int [] intArray;
    intArray = new int [5];
    
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    intArray [2] = 1000;
    intArray [3] = 500;
    
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten
    double [] doubleArray = {1.1,2.2,3.3};


        
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    //System.out.println(doubleArray[2]);
    
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    
    System.out.println("An welchen Index soll der neue Wert?");
    
    
    System.out.println("Geben Sie den neuen Wert f�r index " +   doubleArray[2]   + " an:");
    
    
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // __   __  __  __  __  
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    
    
    
    
    
    
    //AUFGABE 1
    
    int [] myArray = {1,2,3,4,5,6,7,8,9,10};
    
    for (int i = 0; i < 10; i++) {
    System.out.println(myArray[i]);
    }
    
    myArray[3] = 1000;
    System.out.println(myArray[3]);
   
    for (int i = 0; i < 10; i++) {
     myArray[i] =0;
    }
    
    System.out.print("W�hlen Sie eine Aktion aus: ");
    System.out.print("1: Alle Werte ausgeben ");
    System.out.print("2: Ein bestimmtem Wert ausgeben ");
    System.out.print("3: Das Array komplett mit neuen Werten bef�llen ");
    System.out.print("4: Einen bestimmten Wert �ndern ");
    
    int auswahl = 0;
    auswahl = sc.nextInt();
    
    switch(auswahl) {
    case 1:
    System.out.println("Nimm den Schirm mit!");
    break;
    
    case 2:
    System.out.println("Be happy!");
    break;
    
    case 3:
    
    break;
    
    case 4:
    	
    break;
    
    default:
    System.out.println("ERROR");
    }
    
    
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung

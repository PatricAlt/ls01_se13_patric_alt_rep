
public class class1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//AUFGABE1

		String stern1 = "*";
		String stern2 = "**";
		
		System.out.printf("%4s\n", stern2);
		System.out.printf(stern1);
		System.out.printf("%5s",stern1);
		System.out.printf("\n"+stern1+"%5s",stern1); //Zeile 11 und 12 Kombiniert
		System.out.printf("\n%4s", stern2);
		
		System.out.printf("\n\n\n", stern2);
		//AUFGABE2
		
		String maldinger = " 1 * 2 * 3 * 4 *5 ";
		
		System.out.printf("%-5s"+"="+"%-19s"+"="+"%4s" , "0!", "", "1");
		System.out.println("");
		System.out.printf("%-5s"+"="+"%-19.2s"+"="+"%4s" , "1!", maldinger, "1");
		System.out.println("");
		System.out.printf("%-5s"+"="+"%-19.7s"+"="+"%4s" , "2!", maldinger, "2");
		System.out.println("");
		System.out.printf("%-5s"+"="+"%-19.10s"+"="+"%4s" , "3!", maldinger, "6");
		System.out.println("");
		System.out.printf("%-5s"+"="+"%-19.15s"+"="+"%4s" , "4!", maldinger, "24");
		System.out.println("");		
		System.out.printf("%-5s"+"="+"%-19s"+"="+"%4s" , "5!", maldinger, "120");
		
		//System.out.println("12345=1234567890123456789=1234");//Check um Laengen gleich sind wie in aufgabe (5=19=4)
		
		System.out.println("\n");
		System.out.println("\n");
		
		//AUFGABE 3
		//Temperaturtabelle
		

		int fahrenheit = -20;		
		double celsius = -28.8889;
		System.out.printf("%-12s"+"|"+"%10s", "Fahrenehit", "Celsius");
		System.out.println("");
		
		System.out.println("-----------------------");	
				
		System.out.printf("%-12d"+"|"+"%10.2f", fahrenheit, celsius);
		System.out.println("");
		fahrenheit = -10;		
		celsius = -23.3333;
				
		System.out.printf("%-12d"+"|"+"%10.2f", fahrenheit, celsius);
		System.out.println("");		
		fahrenheit = 0;		
		celsius = -17.7778;
		
		System.out.printf("+%-11d"+"|"+"%10.2f",+fahrenheit, celsius);
		System.out.println("");	
		fahrenheit = 20;		
		celsius = -6.6667;
		
		System.out.printf("+%-11d"+"|"+"%10.2f", fahrenheit, celsius);
		System.out.println("");	
		fahrenheit = 30;		
		celsius = -1.1111;
		
		System.out.printf("+%-11d"+"|"+"%10.2f", fahrenheit, celsius);
		System.out.println("");			
		
		//System.out.print("123456789012|1234567890");//check um Laengen gleich sind wie in aufgabe 
		
		System.out.println("");	
		System.out.println("");	
		
		
	}

}

import java.util.Scanner; // Import der Klasse Scanner
public class Konsoleneingabe
{

 public static void main(String[] args) // Hier startet das Programm
 {
	 Scanner myscanner = new Scanner (System.in);
	 
	 System.out.println("Guten Tag");
	 System.out.println("Bitte geben sie Ihren Namen und Alter ein.");
	 
	 System.out.println("Name: ");
	 String name = myscanner.next();
	 
	 System.out.println("Alter: ");
	 int alter = myscanner.nextInt();
	 
	 System.out.println("Ihr Name lautet "+name+" und Sie sind "+alter+" Jahre alt.");
	 
	 myscanner.close();
 }
}